    //empty
    bE0:     array [0..7] of byte = ( $00,$00,$00,$00,$00,$00,$00,$00 );
    //stripes
    bS0:     array [0..7] of byte = ( $00,$41,$00,$42,$00,$00,$40,$00 );
    bS1:     array [0..7] of byte = ( $00,$42,$00,$40,$41,$00,$42,$00 );
    bS2:     array [0..7] of byte = ( $00,$00,$40,$00,$00,$42,$00,$00 );
    bS3:     array [0..7] of byte = ( $40,$41,$00,$00,$40,$00,$42,$00 );
    //floors
    bF0:     array [0..7] of byte = ( $01,$01,$01,$01,$01,$01,$01,$01 );
    bF1:     array [0..7] of byte = ( $01,$02,$01,$02,$01,$02,$01,$02 );
    bF2:     array [0..7] of byte = ( $02,$02,$02,$02,$02,$02,$02,$02 );
    bF3:     array [0..7] of byte = ( $03,$02,$03,$02,$03,$02,$03,$02 );
    bF4:     array [0..7] of byte = ( $03,$03,$03,$03,$03,$03,$03,$03 );
    bF5:     array [0..7] of byte = ( $01,$06,$01,$06,$01,$06,$01,$06 );
    // holes
    bH5:     array [0..7] of byte = ( $01,$06,$00,$00,$00,$00,$00,$06 );
    // pipe
    bP0:     array [0..7] of byte = ( $01,$04,$05,$01,$02,$01,$04,$05 );
    bP1:     array [0..7] of byte = ( $00,$20,$21,$22,$23,$24,$25,$26 );
    // lamps
    bL0:     array [0..7] of byte = ( $00,$00,$43,$44,$44,$45,$00,$00 );
    bL1:     array [0..7] of byte = ( $00,$00,$46,$47,$48,$00,$00,$00 );
    bL2:     array [0..7] of byte = ( $00,$00,$49,$00,$00,$00,$49,$00 );
    // column
    bC2:     array [0..7] of byte = ( $00,$00,$00,$4a,$4b,$00,$00,$00 );
    bC1:     array [0..7] of byte = ( $00,$00,$00,$2a,$2b,$00,$00,$00 );
    bC0:     array [0..7] of byte = ( $01,$01,$01,$0a,$0b,$01,$01,$01 );
    // closet
    bb1:     array [0..7] of byte = ( $00,$00,$27,$28,$28,$29,$00,$00 );
    bb0:     array [0..7] of byte = ( $01,$01,$07,$08,$08,$09,$01,$01 ); 
    
    lineTopF0: array [0..31] of pointer = (
        @bS0,@bS1,@bS2,@bS3,@bS2,@bS1,@bS2,@bL2,@bS3,@bS1,@bS3,@bS0,@bS1,@bS0,@bS3,@bS2,
        @bS2,@bS1,@bS2,@bS0,@bS1,@bS0,@bS3,@bS2,@bS3,@bS1,@bS3,@bL2,@bS0,@bS1,@bS2,@bS3
    );
    lineMidPipes: array [0..31] of pointer = (
        @bE0,@bE0,@bE0,@bP1,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bp1,@bE0,@bE0,@bE0,
        @bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bP1,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0
    );
    lineBottomPipes: array [0..31] of pointer = (
        @bF0,@bF1,@bF2,@bP0,@bF4,@bF3,@bF3,@bF2,@bF2,@bF1,@bF1,@bF0,@bp0,@bF2,@bF1,@bF0,
        @bF1,@bF2,@bF2,@bF2,@bF3,@bF3,@bF3,@bF4,@bP0,@bF4,@bF3,@bF3,@bF3,@bF2,@bF2,@bF1
    );
  
    lineTopF1: array [0..31] of pointer = (
        @bs0,@bL0,@bE0,@bL0,@bE0,@bS1,@bS2,@bE0,@bE0,@bL0,@bE0,@bL0,@bE0,@bs1,@bs2,@bE0,
        @bE0,@bL0,@bE0,@bL0,@bE0,@bs3,@bs0,@bs1,@bE0,@bL0,@bs0,@bL0,@bE0,@bs2,@bs1,@bE0
    );

    lineMidF1: array [0..31] of pointer = (
        @bE0,@bE0,@bb1,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bb1,@bE0,
        @bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bb1,@bE0,@bE0,@bE0,@bE0,@bE0
    );
 
    lineBottomL0F1: array [0..31] of pointer = (
        @bF5,@bH5,@bb0,@bF5,@bF5,@bF5,@bH5,@bF5,@bF5,@bF5,@bF5,@bF5,@bF5,@bH5,@bb0,@bH5,
        @bF5,@bF5,@bF5,@bH5,@bF5,@bF5,@bF5,@bF5,@bH5,@bF5,@bb0,@bF5,@bF5,@bF5,@bF5,@bF5
    );

    lineTopF2: array [0..31] of pointer = (
        @bc2,@bE0,@bL1,@bE0,@bs0,@bE0,@bL1,@bE0,@bs2,@bc2,@bL1,@bE0,@bs0,@bE0,@bL1,@bc2,
        @bE0,@bL1,@bE0,@bc2,@bc2,@bL1,@bE0,@bS3,@bE0,@bL1,@bs1,@bc2,@bE0,@bL1,@bE0,@bs0
    );

    lineMidF2: array [0..31] of pointer = (
        @bc1,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bc1,@bE0,@bE0,@bE0,@bE0,@bE0,@bc1,
        @bE0,@bE0,@bE0,@bc1,@bc1,@bE0,@bE0,@bE0,@bE0,@bE0,@bE0,@bc1,@bE0,@bE0,@bE0,@bE0
    );

    lineBottomL0F2: array [0..31] of pointer = (
        @bc0,@bF5,@bF5,@bF5,@bH5,@bF5,@bF5,@bF5,@bH5,@bc0,@bF5,@bF5,@bF5,@bF5,@bH5,@bc0,
        @bF5,@bF5,@bF5,@bc0,@bc0,@bH5,@bF5,@bF5,@bF5,@bF5,@bH5,@bc0,@bF5,@bF5,@bH5,@bH5
    );

    itemMap: array [0..2,0..31] of byte;

    stripeL0F2: array[0..2] of pointer = (@lineTopF2,@lineMidF2,@lineBottomL0F2);
    stripeL0F1: array[0..2] of pointer = (@lineTopF1,@lineMidF1,@lineBottomL0F1);
    stripeL0F0: array[0..2] of pointer = (@lineTopF0,@lineMidPipes,@lineBottomPipes);

    level: array [0..2] of pointer = (@stripeL0F2,@stripeL0F1,@stripeL0F0);
