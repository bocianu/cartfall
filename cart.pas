program cartfall;
uses atari, rmt, joystick, aplib, graph;
const
{$i const.inc}
{$r resources.rc}
{$i sprites.inc}
{$i interrupts.inc}

var
    b:byte;
    x:word;
    tx,ty,tf,td,tdx,lf: byte;
    sprDX:shortint;
    tfloor: byte;
    fallfrom: byte;
    tspeed: byte;
    tvspeed: byte;
    tyd, tdy: byte;
    tvdir: byte;
    jumpForce:byte;
    turnframe: byte;
    tdir:byte;
    tstate:byte;
    bupdate,tupdate:boolean;
    iframe:byte;
    msx: TRMT;
    floorY: array [0..2] of byte = ( 80, 55, 30);

    sprN: array [0..2] of byte = (NONE, NONE, NONE);
    sprX: array [0..2] of byte = (0,0,0);

    timeBar: Pchar = 'TIME:                                   '~;
    ScoreBar: Pchar = 'MONEY: 0000                             '~;
    gamaOverBar: Pchar = '              GAME  OVER                '*~;

    scoreMap: array [0..2] of byte = (1,2,5);
    time: word;
    timeLen: byte;
    game_over: boolean;
    score: word;
    scoreadd: byte;

    KOLP0P: byte absolute $d00c;
    KOLP1P: byte absolute $d00d;
    
{$i level.inc}    
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

procedure ClrBars;
begin
    FillByte(pointer(TOP_ADDRESS),$80,0);
end;

procedure ShowTime;
begin
    timeLen := time shr 6;
    move(timeBar,pointer(BOTTOM_ADDRESS),40);
    FillChar(pointer(BOTTOM_ADDRESS+5),timeLen,' '*~);
end;

procedure ShowScore;
begin
    move(scoreBar,pointer(TOP_ADDRESS),40);
end;

procedure DrawItem(floor:byte);
var 
    frames0:array [0..0] of pointer;
    frames1:array [0..0] of pointer;
begin
    frames0:=items0[sprN[floor]];
    frames1:=items1[sprN[floor]];
    move(frames0[iframe],pointer(PMG_ADDRESS+$300+floorY[floor]),8);
    move(frames1[iframe],pointer(PMG_ADDRESS+$380+floorY[floor]),8);
end;

procedure PutItem(item,floor,xpos:byte);
begin
    sprX[floor]:=xpos;
    sprN[floor]:=item;
end;

procedure ClearItem(floor:byte);
begin
    sprX[floor]:=0;
    sprN[floor]:=NONE;
    FillByte(pointer(PMG_ADDRESS+$300+floorY[floor]),8,0);
    FillByte(pointer(PMG_ADDRESS+$380+floorY[floor]),8,0);   
end;

procedure ClearItemMap;
begin
    FillByte(@itemMap, 3* 32, NONE);
end;

procedure ClearFloor(floor:byte);
begin
    FillByte(@itemMap[floor, 0],32,NONE); 
    ClearItem(floor);
end;

procedure RandomCoin(floor:byte);
begin
    b:=((x shr 5)+6+Random(26)) and 31;
    itemMap[floor, b]:=Random(3);
end;

procedure PMGInit();inline;
begin
    FillByte(pointer(PMG_ADDRESS+$180),$280,0);
    pmbase := Hi(PMG_ADDRESS);
    gractl := %00000000;
    gprior := %00110001;
    sdmctl := %0;
    pcolr0 := $9a;
    pcolr1 := $9a;
    pcolr2 := $f6;
    pcolr3 := $fa;
    sizep0 := 0;
    sizep1 := 0;
    sizep2 := 0;
    sizep3 := 0;
end;

procedure PMGOff();inline;
begin
    FillByte(pointer(PMG_ADDRESS+$180),$280,0);
    gractl := %00000000;
    gprior := %00110001;
    sdmctl := %0;
end;

procedure PutTrolley();
begin
    FillByte(pointer(PMG_ADDRESS+$200+ty-5),26,0);
    FillByte(pointer(PMG_ADDRESS+$280+ty-5),26,0);
    Move(frames0[tf],pointer(PMG_ADDRESS+$200+ty),16);
    Move(frames1[tf],pointer(PMG_ADDRESS+$280+ty),16);
    hposp0:=tx;
    hposp1:=tx+8;

end;

procedure PutStripe(stripenum:byte);
var i,s,l:byte;
    vram,vram2:word;
    blocknum:byte;
    stripe:array [0..0] of pointer;
    line: array [0..0] of pointer;
    block: array [0..0] of byte;
begin
    blocknum := stripenum and 31;
    stripenum := stripenum shl 3;
    
    vram := VRAM_ADDRESS + (stripenum and 63);
    
    for i := 0 to 7 do begin
        
        for s:=0 to 2 do begin
            stripe := level[s];
            for l:=0 to 2 do begin
                line := stripe[l];
                block := line[blocknum];
                poke(vram, block[i]);
                inc(vram,$40);
                poke(vram, block[i]);
                inc(vram,$40);
            end;
    
        end; 
        Dec(vram,$47f);
        Dec(vram2,$47f);
    end;
end;

procedure DrawEdges(xbyte:byte);
var newStripe:byte;
    xpos:byte;
begin
    xpos:=LEFT_SPAWN;
    if tdir and DIR_RIGHT = 0 then newStripe := (xbyte-1);
    if tdir and DIR_LEFT = 0 then begin
        newStripe := (xbyte+5);
        xpos:=RIGHT_SPAWN;
    end;
    PutStripe(newStripe);
    newStripe := newStripe and 31;
    for b:=0 to 2 do 
        if itemMap[b, newStripe] <> NONE then PutItem(itemMap[b, newStripe],b,xpos)
end;

procedure SetViewPos(x:word);
var off, vram:word;
    i,xbyte,xbit:byte;
    dloff:array[0..8]of byte = (3,3,4,3,3,4,3,3,0);
begin
    xbyte := (Lo(x) shr 2);
    xbit := x and 3;
    hscrol := 8 - xbit;
    off := DLIST_ADDRESS + 8;
    vram := (VRAM_ADDRESS + xbyte) - 2;
    for i:=0 to 8 do begin
        Dpoke(off, vram);
        Inc(off, dloff[i]);
        Inc(vram, $80);
    end;
    if ((xbyte and 7) = 0) and (xbit < 2) then DrawEdges(x shr 5);
end;

procedure WaitForFire;
begin
    repeat pause until strig0 = 0;
    repeat until strig0 = 1;
end;

procedure ShowTitle;
begin
    pause;
    nmien:=$40;
    PMGOff();
    InitGraph(8+16);
    sdmctl:=0;
    Pause;
    color1:=12;
    color2:=0;
    unApl(pointer(TITLE_GFX),pointer(savmsc));
    sdmctl := %00100010;
    WaitForFire;
end;

procedure GameInit();
begin
    td:=0;
    tdx:=0;
    tyd:=0;
    x:=0;
    tf:=0;
    lf:=NONE;
    tx:=116;
    tfloor := 1;
    fallfrom := tfloor;
    jumpForce := 0;
    ty:=floorY[tfloor];
    tdir:=DIR_RIGHT;
    tstate:=TROLLEY_RUN;
    tspeed:=0;
    tvspeed:=0;
    time:=2240;
    game_over:=false;
    score:=0;
    scoreadd:=0;
end;

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

begin

    Randomize;
    SetIntVec(iVBL, @vbl);
    SetIntVec(iDLI, @dli);
    msx.player := pointer(RMT_PLAYER_ADDRESS);
    msx.modul := pointer(RMT_MODULE_ADDRESS);
    msx.Init(0);

    repeat
        ShowTitle;

        // INIT GAME SCREEN
        Pause;
        SDLSTL := DLIST_ADDRESS;
        nmien := $c0; // set $80 for dli only (without vbl)

        PMGInit();        
        GameInit();
        ClrBars();
        ClearItemMap();
        for b:=0 to 4 do PutStripe(b);
        PutTrolley();
        RandomCoin(0);
        RandomCoin(1);
        RandomCoin(2);
        SetViewPos(x);  
        ShowTime();
        ShowScore;
        sdmctl := %00101110;
        gractl := %00000011;
        Pause();
        
        // MAIN GAME LOOP
        
        repeat
            bupdate:=false;
            tupdate:=false;
            hitclr:=$ff;
            
            // CHECK JOYSTICK AND REACT
                    
            if tstate = TROLLEY_RUN then begin
                tf := tf and %11000;
                if tspeed<16 then inc(tspeed);
                tvspeed := 0;   
                if (stick0 = joy_right) and (tdir = DIR_LEFT) then begin
                    tstate:=TROLLEY_TURN;
                    turnframe:=4;
                end;
                if (stick0 = joy_left) and (tdir = DIR_RIGHT) then begin
                    tstate:=TROLLEY_TURN;
                    turnframe:=4;
                end;
                if (strig0 = 0) then begin
                    tf := (tf and %11000) + 1;
                    if jumpForce<20 then inc(jumpForce);
                    
                end;
                if (strig0 = 1) and (jumpForce > 0) then begin
                    tvdir := DIR_UP;
                    tvspeed := jumpForce and 31;
                    tstate := TROLLEY_JUMP;
                    tyd := 0;
                    jumpForce := 0;
                    tf := (tf and %11000) + 2;
                    fallfrom:= tfloor;
                end;
             
            end;
            
            // ANIMATE ON TURNING
            
            if tstate = TROLLEY_TURN then begin
                if tspeed > 2 then tspeed := tspeed - 2;
                turnframe := turnframe+1;
                tf:= (tf and 8) + (turnframe shr 1);
                if (tf and 7) = 0 then begin
                    tf := tf and 15;
                    tstate := TROLLEY_RUN;
                    tdir := tdir xor 3;
                    td:=0;
                end;
            end;
            
            // ON FALLING CHECK IF NOT HIT THE GROUND
            
            if tstate = TROLLEY_FALL then begin
                for b:=2 downto 0 do 
                    if (fallfrom=b+1) and (ty>=floorY[b]) then begin 
                        tstate := TROLLEY_RUN;
                        ty:=floorY[b];
                        tfloor:=b;
                        tvspeed:=0;
                        tf := tf and %11000 + 1;
                    end;
            end;
            
            // ON JUMP CHECK IF NOT START FALLING

            if (tstate = TROLLEY_JUMP) and (tvspeed = 0) then begin
                    tf := (tf and %11000)+2;
                    tvdir := DIR_DOWN;
                    tstate := TROLLEY_FALL;
                    tvspeed := 1;
                    tyd := 0;
                    for b:=0 to 2 do 
                        if  (ty<=floorY[b]) then fallfrom:=b+1;
            end;
            
            // HORIZONTAL MOVEMENT

            td := td + tspeed;
            tdx := td shr 3;
            sprDX := 0;
            if tdx > 0 then begin
                bupdate := true;
                if tdir = DIR_RIGHT then begin
                    x := x + tdx;
                    sprDX := -tdx;
                end;
                if tdir = DIR_LEFT then begin
                    x := x - tdx;
                    sprDX := tdx;
                end;
                td := td and 7;
                for b:=0 to 2 do 
                    if sprN[b]<>NONE then begin
                        sprX[b] := sprX[b] + sprDX;
                        if (sprX[b] < LEFT_SPAWN) or (sprX[b]>RIGHT_SPAWN) then ClearItem(b);
                    end;
            end;
            
            // VERTICAL MOVEMENT
            
            if (tvspeed > 0) then begin
                tyd := tyd + tvspeed;
                tdy := tyd shr 3;
                if tdy > 0 then begin
                    tupdate := true;
                    if tvdir = DIR_DOWN then ty:=ty+tdy;
                    if tvdir = DIR_UP then ty:=ty-tdy;
                    tyd := tyd and 7;
                end;
                if tvdir = DIR_DOWN then if tvspeed<7 then inc(tvspeed);
                if tvdir = DIR_UP then if tvspeed>0 then dec(tvspeed);
            end;
            
            // SHOW SCREEN CHANGES IF NECESSARY
            
            if (lf<>tf) or tupdate then PutTrolley();
            lf:=tf;
            if bupdate then SetViewPos(x);
            for b:=0 to 2 do 
                if sprN[b]<>NONE then DrawItem(b);
            
            Pause();

            // PLAYER AND HOLE COLLISION

            if tstate = TROLLEY_RUN then begin
                if ((HPOSM0 or HPOSM1) and 3) = 0 then begin
                    tstate := TROLLEY_FALL;
                    tf := (tf and %11000)+2;
                    tvdir := DIR_DOWN;
                    tvspeed := 1;
                    tyd := 0;
                    fallfrom := tfloor;
                end
            end;        

            // PLAYER AND COIN COLLISION 
            
            if (KOLP0P or KOLP1P) and %1100 <> 0 then begin
               tfloor:=NONE;
               for b:=0 to 2 do if (ty<=floorY[b]) then tfloor:=b;
               if (tfloor <> NONE) and (sprN[tfloor]<>NONE) then begin 
                   scoreadd := scoreMap[sprN[tfloor]];
                   asm
                      txa
                      pha               
                      jsr addScore
                      pla 
                      tax
                   end;
                   ClearFloor(tfloor);
                   RandomCoin(tfloor);
                end;
            end;
            
            // ANIMATE COIN

            if rtclok3 and 3 = 0 then begin
                Inc(iframe);
                if iframe > 5 then iframe := 0;
            end;
            
            // CHECK TIME
        
            Dec(time);
            if timeLen <> (time shr 6) then ShowTime;
            if time = 0 then game_over := true;
            //color4:=$0;
        
        until game_over;
        
        move(gamaOverBar,pointer(BOTTOM_ADDRESS+40),40);   
        pause(50);
        WaitForFire;
        sdmctl:=0;
        
    until false;

    msx.play;
end.
