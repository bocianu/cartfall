#!/bin/bash
for IMAGE in $(ls -1 *.gif)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    magick convert $IMAGE  -depth 1 $NAME.gray
    cp $NAME.gray $NAME.gfx
    ./apultra.exe $NAME.gfx $NAME.apu
    rm $NAME.gray* 

done
