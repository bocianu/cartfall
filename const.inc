//
// ;*** define your project wide constants here
// 
// ;*** I like to keep memory locations at top of this file
//



TOP_ADDRESS = $6000;
BOTTOM_ADDRESS = $6000+40;
VRAM_ADDRESS = $6080;
DLIST_ADDRESS = $6700;
CHARSET_ADDRESS = $7000;
PMG_ADDRESS = $7400;
TITLE_GFX = $7800;

RMT_PLAYER_ADDRESS = $5800;
RMT_MODULE_ADDRESS = $5D00;

// ;*** and here goes all other stuff

NONE = $ff;

DIR_LEFT = 1;
DIR_RIGHT = 2;
DIR_UP = 1;
DIR_DOWN = 2;

TROLLEY_RUN = 0;
TROLLEY_TURN = 1;
TROLLEY_JUMP = 2;
TROLLEY_FALL = 3;


LEFT_SPAWN = 28;
RIGHT_SPAWN = 220;
