(* declare your interrupt routines here *)

procedure dli;assembler;interrupt;
asm {
    pha ; store registers

    mva #0 colpf2
    mva #$e0 chbase
    mwa #dli1 atari.VDSLST
    
    pla ; restore registers
};
end;

procedure dli1;assembler;interrupt;
asm {
    pha ; store registers

    mva #$70 chbase
    mva #$b2 colpf2
    mva #$b6 colpf1
    mva #$b8 colpf0

    lda adr.sprx+2
    sta hposp2
    sta hposp3

    mwa #dli2 atari.VDSLST
    
    pla ; restore registers
};
end;

procedure dli2;assembler;interrupt;
asm {
    pha ; store registers

    mva #$12 colpf2
    mva #$16 colpf1
    mva #$18 colpf0

    lda adr.sprx+1
    sta hposp2
    sta hposp3

    mwa #dli3 atari.VDSLST
    
    pla ; restore registers
};
end;

procedure dli3;assembler;interrupt;
asm {
    pha ; store registers

    mva #$82 colpf2
    mva #$86 colpf1
    mva #$88 colpf0

    lda adr.sprx
    sta hposp2
    sta hposp3

    mwa #dli4 atari.VDSLST
    
    pla ; restore registers
};
end;

procedure dli4;assembler;interrupt;
asm {
    pha ; store registers

    mva #0 colpf2
    mva #$e0 chbase
    mwa #dli atari.VDSLST
    
    pla ; restore registers
};
end;

procedure vbl;assembler;interrupt;
asm {
    phr ; store registers
    
;   *** example test routine    
;    mva 20 atari.colbk // blink background
    
;   *** RMT play routine
    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY

    mwa #dli atari.VDSLST

    plr ; restore registers
    jmp $E462 ; jump to system VBL handler
    
    
.def :putnum
    and #%1111
    ora #16
    sta TOP_ADDRESS,y;
    dey
    rts

.def :printScore
    ldy #10
    ldx #0
@              
    lda score,x 
    pha
    jsr putnum
    pla
    :4 lsr 
    jsr putnum
    inx
    cpx #2
    bne @-
    rts
    
       
.def :addScore

    sed
    clc
    lda score
    adc scoreadd
    sta score
    lda score+1
    adc #0
    sta score+1
    cld

    jsr printScore

    rts
      
    
    
};
end;
