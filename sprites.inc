// sprite 0 data
  frames0_0: array [0..$0f] of byte = (
    $c0, $30, $18, $0c, $0f, $0a, $0d, $06, $05, $06, $07, $04, $07, $0c, $14, $08
  );
  frames0_1: array [0..$0f] of byte = (
    $00, $c0, $30, $18, $0c, $0f, $0a, $0d, $06, $05, $06, $07, $07, $0f, $14, $08
  );
  frames0_2: array [0..$0f] of byte = (
    $30, $18, $0c, $0e, $07, $06, $05, $06, $05, $06, $07, $04, $0f, $14, $08, $00
  );
  frames0_3: array [0..$0f] of byte = (
    $1e, $1b, $0d, $06, $07, $07, $05, $06, $05, $06, $07, $05, $07, $07, $05, $00
  );
  frames0_4: array [0..$0f] of byte = (
    $0f, $08, $04, $06, $07, $06, $07, $07, $06, $07, $07, $04, $07, $04, $07, $05
  );
  frames0_5: array [0..$0f] of byte = (
    $03, $05, $04, $04, $04, $07, $08, $08, $0f, $0a, $05, $06, $07, $04, $03, $04
  );
  frames0_6: array [0..$0f] of byte = (
    $03, $06, $04, $0c, $1f, $10, $1f, $1a, $15, $1f, $0f, $04, $07, $08, $0f, $08
  );
  frames0_7: array [0..$0f] of byte = (
    $00, $00, $01, $01, $03, $3f, $3e, $3d, $1e, $1d, $0f, $00, $03, $1f, $1c, $14
  );
  frames0_8: array [0..$0f] of byte = (
    $00, $00, $00, $00, $ff, $d5, $6a, $55, $2a, $35, $1f, $00, $1f, $30, $28, $10
  );
  frames0_9: array [0..$0f] of byte = (
    $00, $00, $00, $00, $00, $ff, $d5, $6a, $55, $2a, $35, $1f, $1f, $3f, $28, $10
  );
  frames0_10: array [0..$0f] of byte = (
    $00, $00, $00, $00, $01, $1f, $ea, $55, $2a, $35, $1f, $00, $0f, $18, $14, $08
  );

// sprite 1 data
  frames1_0: array [0..$0f] of byte = (
    $00, $00, $00, $00, $ff, $ab, $56, $aa, $54, $ac, $f8, $00, $f8, $0c, $14, $08
  );
  frames1_1: array [0..$0f] of byte = (
    $00, $00, $00, $00, $00, $ff, $ab, $56, $aa, $54, $ac, $f8, $f8, $fc, $14, $08
  );
  frames1_2: array [0..$0f] of byte = (
    $00, $00, $00, $00, $80, $f8, $57, $aa, $54, $ac, $f8, $00, $f0, $18, $28, $10
  );
  frames1_3: array [0..$0f] of byte = (
    $00, $00, $80, $80, $c0, $fc, $7c, $bc, $78, $b8, $f0, $00, $c0, $f8, $38, $28
  );
  frames1_4: array [0..$0f] of byte = (
    $c0, $60, $20, $30, $f8, $08, $f8, $58, $a8, $f8, $f0, $20, $e0, $10, $f0, $10
  );
  frames1_5: array [0..$0f] of byte = (
    $e0, $10, $10, $10, $10, $f0, $08, $08, $f8, $a8, $50, $b0, $f0, $10, $e0, $10
  );
  frames1_6: array [0..$0f] of byte = (
    $f0, $10, $20, $60, $e0, $60, $e0, $e0, $60, $e0, $e0, $20, $e0, $20, $e0, $a0
  );
  frames1_7: array [0..$0f] of byte = (
    $78, $d8, $b0, $60, $e0, $e0, $a0, $60, $a0, $60, $e0, $a0, $e0, $e0, $a0, $00
  );
  frames1_8: array [0..$0f] of byte = (
    $03, $0c, $18, $30, $f0, $50, $b0, $60, $a0, $60, $e0, $20, $e0, $30, $28, $10
  );
  frames1_9: array [0..$0f] of byte = (
    $00, $03, $0c, $18, $30, $f0, $50, $b0, $60, $a0, $60, $e0, $e0, $f0, $28, $10
  );
  frames1_10: array [0..$0f] of byte = (
    $0c, $18, $30, $70, $e0, $60, $a0, $60, $a0, $60, $e0, $20, $f0, $28, $10, $00
  );
  
  
  frames0: array [0..$0f] of pointer = (
    @frames0_0, @frames0_1, @frames0_2, @frames0_3, @frames0_4, @frames0_5, @frames0_6, @frames0_7, 
    @frames0_8, @frames0_9, @frames0_10, @frames0_7, @frames0_6, @frames0_5, @frames0_4, @frames0_3
  );
  frames1: array [0..$0f] of pointer = (
    @frames1_0, @frames1_1, @frames1_2, @frames1_3, @frames1_4, @frames1_5, @frames1_6, @frames1_7, 
    @frames1_8, @frames1_9, @frames1_10, @frames1_7, @frames1_6, @frames1_5, @frames1_4, @frames1_3
  );
  
  

// sprite 0 data
  coin1frames0_0: array [0..$07] of byte = (
    $24, $42, $38, $18, $18, $bd, $81, $66
  );
  coin1frames0_1: array [0..$07] of byte = (
    $2c, $46, $30, $12, $12, $3a, $02, $04
  );
  coin1frames0_2: array [0..$07] of byte = (
    $08, $0c, $10, $14, $14, $14, $00, $24
  );
  coin1frames0_3: array [0..$07] of byte = (
    $10, $10, $08, $08, $18, $08, $00, $08
  );
  coin1frames0_4: array [0..$07] of byte = (
    $10, $30, $08, $28, $28, $28, $00, $24
  );
  coin1frames0_5: array [0..$07] of byte = (
    $34, $62, $18, $58, $58, $5c, $40, $20
  );

// sprite 1 data
  coin1frames1_0: array [0..$07] of byte = (
    $18, $7e, $c7, $e7, $e7, $c3, $7e, $3c
  );
  coin1frames1_1: array [0..$07] of byte = (
    $18, $3c, $4e, $6e, $6e, $46, $7e, $3c
  );
  coin1frames1_2: array [0..$07] of byte = (
    $18, $3c, $2c, $2c, $2c, $2c, $3c, $18
  );
  coin1frames1_3: array [0..$07] of byte = (
    $08, $08, $18, $18, $18, $18, $18, $10
  );
  coin1frames1_4: array [0..$07] of byte = (
    $18, $3c, $34, $34, $34, $34, $3c, $18
  );
  coin1frames1_5: array [0..$07] of byte = (
    $18, $3c, $66, $76, $76, $62, $7e, $3c
  );
  
// sprite 0 data
  coin2frames0_0: array [0..$07] of byte = (
    $24, $42, $38, $0c, $30, $bd, $81, $66
  );
  coin2frames0_1: array [0..$07] of byte = (
    $2c, $46, $38, $0a, $1a, $3a, $02, $04
  );
  coin2frames0_2: array [0..$07] of byte = (
    $08, $0c, $10, $0c, $14, $1c, $00, $24
  );
  coin2frames0_3: array [0..$07] of byte = (
    $10, $10, $18, $08, $18, $08, $00, $08
  );
  coin2frames0_4: array [0..$07] of byte = (
    $10, $30, $10, $28, $30, $38, $00, $24
  );
  coin2frames0_5: array [0..$07] of byte = (
    $34, $62, $18, $44, $48, $5c, $40, $20
  );

// sprite 1 data
  coin2frames1_0: array [0..$07] of byte = (
    $18, $7e, $c7, $f3, $cf, $c3, $7e, $3c
  );
  coin2frames1_1: array [0..$07] of byte = (
    $18, $3c, $4e, $76, $6e, $46, $7e, $3c
  );
  coin2frames1_2: array [0..$07] of byte = (
    $18, $3c, $2c, $34, $2c, $24, $3c, $18
  );
  coin2frames1_3: array [0..$07] of byte = (
    $08, $08, $08, $18, $18, $18, $18, $10
  );
  coin2frames1_4: array [0..$07] of byte = (
    $18, $3c, $2c, $34, $2c, $24, $3c, $18
  );
  coin2frames1_5: array [0..$07] of byte = (
    $18, $3c, $66, $7a, $76, $62, $7e, $3c
  );


// sprite 0 data
  coin5frames0_0: array [0..$07] of byte = (
    $24, $42, $3c, $38, $0c, $b9, $81, $66
  );
  coin5frames0_1: array [0..$07] of byte = (
    $2c, $46, $38, $3a, $0a, $3a, $02, $04
  );
  coin5frames0_2: array [0..$07] of byte = (
    $08, $0c, $18, $14, $0c, $14, $00, $24
  );
  coin5frames0_3: array [0..$07] of byte = (
    $10, $10, $08, $08, $18, $08, $00, $08
  );
  coin5frames0_4: array [0..$07] of byte = (
    $10, $30, $18, $30, $28, $30, $00, $24
  );
  coin5frames0_5: array [0..$07] of byte = (
    $34, $62, $1c, $58, $4c, $58, $40, $20
  );

// sprite 1 data
  coin5frames1_0: array [0..$07] of byte = (
    $18, $7e, $c3, $c7, $f3, $c7, $7e, $3c
  );
  coin5frames1_1: array [0..$07] of byte = (
    $18, $3c, $46, $4e, $76, $4e, $7e, $3c
  );
  coin5frames1_2: array [0..$07] of byte = (
    $18, $3c, $24, $2c, $34, $2c, $3c, $18
  );
  coin5frames1_3: array [0..$07] of byte = (
    $08, $08, $18, $18, $18, $18, $18, $10
  );
  coin5frames1_4: array [0..$07] of byte = (
    $18, $3c, $24, $2c, $34, $2c, $3c, $18
  );
  coin5frames1_5: array [0..$07] of byte = (
    $18, $3c, $62, $66, $7a, $66, $7e, $3c
  );


  coin1frames0: array [0..5] of pointer = (
    @coin1frames0_0,@coin1frames0_1,@coin1frames0_2,@coin1frames0_3,@coin1frames0_4,@coin1frames0_5
  );
  coin1frames1: array [0..5] of pointer = (
    @coin1frames1_0,@coin1frames1_1,@coin1frames1_2,@coin1frames1_3,@coin1frames1_4,@coin1frames1_5
  );

  coin2frames0: array [0..5] of pointer = (
    @coin2frames0_0,@coin2frames0_1,@coin2frames0_2,@coin2frames0_3,@coin2frames0_4,@coin2frames0_5
  );
  coin2frames1: array [0..5] of pointer = (
    @coin2frames1_0,@coin2frames1_1,@coin2frames1_2,@coin2frames1_3,@coin2frames1_4,@coin2frames1_5
  );

  coin5frames0: array [0..5] of pointer = (
    @coin5frames0_0,@coin5frames0_1,@coin5frames0_2,@coin5frames0_3,@coin5frames0_4,@coin5frames0_5
  );
  coin5frames1: array [0..5] of pointer = (
    @coin5frames1_0,@coin5frames1_1,@coin5frames1_2,@coin5frames1_3,@coin5frames1_4,@coin5frames1_5
  );

  items0: array [0..2] of pointer = (
    @coin1frames0, @coin2frames0, @coin5frames0
  ); 
  
  items1: array [0..2] of pointer = (
    @coin1frames1, @coin2frames1, @coin5frames1
  ); 
  
  
